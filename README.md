# egg
a simple egg timer with notification capabilities, written in rust

## installation
in the future, I'd like to have this added to various package managers, but for now it's just in this repo.

```
git clone https://gitlab.com/oddx/egg.git
cd egg
cargo build -r
```
and then copy the egg/target/release/egg binary somewhere in your path.
or don't. you do you!

## usage
right now, the project is just a cli util.

`egg minutes<:seconds<:miliseconds>> <notification message>`

so to run a timer to notify you after 20 seconds:
`egg 00:20 "hatched!!"`

`ctrl + c` to exit early

disclaimer: this project is mainly a way for me to learn rust.
in the future, I'd like for this to be cross platform and styled all fancy, but for now we just have this lil mvp
