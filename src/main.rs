#![allow(non_snake_case)]

use dioxus::prelude::*;
use dioxus_tui::TuiContext;
use notify_rust::Notification;
use std::env;
use std::time::{Duration, Instant};

#[derive(Clone)]
struct Egg {
    target: Duration,
    source: Instant,
    notification: Option<String>,
}

fn main() {
    dioxus_tui::launch(app);
}

fn app(cx: Scope) -> Element {
    use_shared_state_provider::<Egg>(cx, initialize);
    let time_left = time_left(cx);

    // padding for duration mismatch
    if time_left <= 1 {
        quit(cx);
    }

    cx.render(rsx! (
        body {
            div {
                Timer {}
            }
        }
    ))
}

fn initialize() -> Egg {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("egg min<:sec:mili> <notification body>")
    }

    Egg {
        target: to_duration(&parse_args(&args[1])),
        source: Instant::now(),
        notification: if args.len() > 2 {
            Some(args[2].to_string())
        } else {
            None
        },
    }
}

// still needs validity check
fn parse_args(str: &str) -> Vec<u64> {
    str.split(':')
        .map(|x| x.parse::<u64>().unwrap())
        .collect::<Vec<u64>>()
}

fn to_duration(slice: &[u64]) -> Duration {
    match slice {
        [x] => Duration::from_secs(x * 60),
        [x, y] => Duration::from_secs(x * 60 + y),
        [x, y, z] => Duration::from_millis((x * 60 * 1000) + (y * 1000) + z),
        [x, y, z, _] => Duration::from_millis((x * 60 * 1000) + (y * 1000) + z),
        _ => panic!("egg min<:sec:mili> <notification body>"),
    }
}

fn time_left(cx: Scope) -> u128 {
    let egg = use_shared_state::<Egg>(cx).unwrap().read();
    egg.source
        .checked_add(egg.target)
        .unwrap_or_else(Instant::now)
        .checked_duration_since(Instant::now())
        .unwrap_or(Duration::ZERO)
        .as_millis()
}

fn format_time(time_left: u128) -> String {
    if time_left == 0 {
        "00:00:00".to_string()
    } else {
        let minutes = time_left / (60 * 1000);
        let seconds = time_left / 1000 % (60);
        let milis = time_left / 10 % 100; // two decimal places

        format!("{:0>2}:{:0>2}:{:0>2}", minutes, seconds, milis)
    }
}

fn Timer(cx: Scope) -> Element {
    let egg = use_shared_state::<Egg>(cx)?;
    let timer = format_time(time_left(cx));

    // creates render loop
    if time_left(cx) > 0 {
        egg.write();
    }

    cx.render(rsx! (
        h1 {
            "{timer}"
        }
    ))
}

fn quit(cx: Scope) {
    let tui_ctx: TuiContext = cx.consume_context().unwrap();
    let egg = use_shared_state::<Egg>(cx).unwrap().read();

    if egg.notification.is_some() {
        Notification::new()
            .summary("🐣 Time's Up! 🐣")
            .body(egg.notification.as_ref().unwrap().as_str())
            .show()
            .unwrap();
    }

    tui_ctx.quit();
}
